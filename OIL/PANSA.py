from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import pandas
import time

def ClickButton(id, t, by='id'):
    if by == 'id':
        element = driver.find_element_by_id(id)
    elif by == 'title':
        element = driver.find_element_by_css_selector("[title^='"+ id +"']")
    driver.execute_script("arguments[0].click();", element)
    time.sleep(t)
    return

def WriteInput(id, txt):
    inputElement = driver.find_element_by_id(id)
    inputElement.send_keys(txt)
    inputElement.send_keys(Keys.ENTER)
    return

def Search(string, t):
    while True:
        try:
            WriteInput('anysearch', string)
            time.sleep(t)
        except NoSuchElementException:
            continue
        break
    time.sleep(t)
    return

def GetInfo(infos):
    selectors = {
    'data':'search-date',
    'titolo':'search-title',
    }

    for news in driver.find_elements_by_class_name('search-content-result'):
        info = {x: news.find_element_by_class_name(selectors[x]).text for x in selectors}
        info['link'] = news.find_element_by_tag_name('a').get_attribute('href')
        #info = { x: str(info[x]) for x in info }
        infos.append(info)
    return infos

def DownloadData(ksrc):
    ClickButton('searchBtn', t)
    Search(ksrc, t)
    # Controllo la presenza di risultati
    results = int(driver.find_element_by_class_name('search-num-result').text.split(' ')[0])
    if results == 0:
        return ("Nessun risultato disponibile")
    # Scarico la lista degli articoli disponibili
    infos = []
    infos = GetInfo(infos)
    # se esistono altre pagine ciclo anche su di loro
    otpag = range(1, (results/12)+1)
    for i in otpag:
        infos = GetInfo(infos)
        driver.execute_script("changeFrom("+str(i*12)+");")
        time.sleep(t)
        print len(infos)
    return infos

def DumpData(infos, title):
    data = pandas.DataFrame().from_dict(infos)
    print len(data)
    print len(data.drop_duplicates(['titolo']))
    data = data.drop_duplicates(['titolo'])
    print len(data)
    data = data[['data', 'titolo', 'link']]
    data.to_csv(title, header=True, index=False, encoding='utf-8')
    return

if __name__ == '__main__':
    t = 4
    driver = webdriver.Firefox()
    driver.get("http://www.ansa.it/")
    data = DownloadData('Cova Viggiano')
    DumpData(data, 'Demo2')
