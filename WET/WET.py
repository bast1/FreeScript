##########################################################################
################# WET - Wallet Ethereum Transactions  ####################
######################        by Bast1      ##############################
################################ GPL v3.0 ################################
##########################################################################
import sys
from bs4 import BeautifulSoup as bs
import urllib2

def loadpage(wallet, x):
    site = 'https://etherscan.io/txsInternal?a='+ wallet +'&p='+ str(x)
    req = urllib2.Request(site, headers={ 'User-Agent': 'Mozilla/5.0' })
    html = urllib2.urlopen(req).read()
    return html


def sumeth(html):
    alls = []
    for x in html.split(' Ether'):
        try:
            value = x.split('</td><td>')[-1]
            value = float(value.replace('<b>.</b>','.'))
            alls.append(value)
        except ValueError:
            continue
    return sum(alls)


def main(wallet):
    x = 1
    tot = 0
    while True:
        value = sumeth(loadpage(wallet, x))
        tot += value
        x += 1
        if value == 0:
            break
    return tot


if __name__ == '__main__':
    if len(sys.argv) is 0:
        print ('Inserire un indirizzo ethereum valido')
    wallet = sys.argv[1]
    tot = main(wallet)
    print (tot)
    sys.exit()
